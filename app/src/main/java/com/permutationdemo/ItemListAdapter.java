package com.permutationdemo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by bhargav on 11/12/17.
 */

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ItemListViewHolder> {

    private Context context;
    private List<String> item;

    public ItemListAdapter(Context context, List<String> item) {
        this.context = context;
        this.item = item;
    }

    @Override
    public ItemListViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_item_layout, viewGroup, false);
        return new ItemListViewHolder(view);

    }

    @Override
    public void onBindViewHolder(ItemListViewHolder holder, int position) {
        holder.tvItemName.setText(item.get(position));
    }

    @Override
    public int getItemCount() {
        return (item!=null?item.size():0);
    }

    public class ItemListViewHolder extends RecyclerView.ViewHolder {

        private TextView tvItemName;
        public ItemListViewHolder(View itemView) {
            super(itemView);
            tvItemName=itemView.findViewById(R.id.tvItemName);
        }
    }
}
