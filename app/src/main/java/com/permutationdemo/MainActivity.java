package com.permutationdemo;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.permutationdemo.util.Constant;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private EditText etEnterWordForComputation;
    private AppCompatButton btnSubmit;


    private ArrayList<String> deleteCharList=new ArrayList<>();
    private ArrayList<String> insertCharGenerateCountList=new ArrayList<>();
    private ArrayList<String> swapCharGenerateList=new ArrayList<>();
    private ArrayList<String> swapAdjacentCharGenerateList=new ArrayList<>();

    private Context context;

    private AppCompatButton btnDeleteChar, btnInsertChar, btnReplaceChar, btnSwapChar,btnNoOfGeneratedWord,btnFormula;

    private ProgressBar progressBar;
    private static final String TAG=MainActivity.class.getName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialization();
        idMappings();

        setEventListener();
    }

    private void initialization() {
        context=this;
    }


    /**
     *  handle Event Listener
     */
    private void setEventListener() {

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkInfoValid()){
                    hideKeyBoard();
                    new FetchDataAsyncTask().execute();
                }
            }
        });

        btnDeleteChar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(deleteCharList!=null && !deleteCharList.isEmpty()) {
                    hideKeyBoard();
                    String formula= "Formula :\nNo. of Char";
                    String title="Delete Char List :"+deleteCharList.size();
                    showDialog(title, formula,deleteCharList);
                }else {
                    Toast.makeText(context,"Please Enter word",Toast.LENGTH_LONG).show();
                }
            }
        });

        btnInsertChar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(insertCharGenerateCountList!=null && !insertCharGenerateCountList.isEmpty()) {
                    hideKeyBoard();
                    String formula= "Formula :\n ((No. of Char+ 1 (insert New Char))) *  alphabet Length";
                    String title="Insert Char List :"+insertCharGenerateCountList.size();

                    showDialog(title, formula, insertCharGenerateCountList);
                }else {
                    Toast.makeText(context,"Please Enter word",Toast.LENGTH_LONG).show();
                }
            }
        });

        btnReplaceChar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(swapCharGenerateList!=null && !swapCharGenerateList.isEmpty()) {
                    hideKeyBoard();
                    String formula= "Formula :\n(No. of Char *  alphabet Length)";
                    String title="Replace Char List :"+swapCharGenerateList.size();

                    showDialog(title, formula, swapCharGenerateList);
                }else {
                    Toast.makeText(context,"Please Enter word",Toast.LENGTH_LONG).show();
                }

            }
        });

        btnSwapChar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(swapAdjacentCharGenerateList!=null && !swapAdjacentCharGenerateList.isEmpty()) {
                    hideKeyBoard();
                    String formula= "Formula :\n(No. of Char - 1)";
                    String title="Swap Char List : "+swapAdjacentCharGenerateList.size();

                    showDialog(title, formula, swapAdjacentCharGenerateList);

                }else {
                    Toast.makeText(context,"Please Enter word",Toast.LENGTH_LONG).show();
                }

            }
        });

        btnNoOfGeneratedWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkInfoValid()){
                    hideKeyBoard();
                    showDialogForCount();
                }

            }
        });

        btnFormula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkInfoValid()){
                    hideKeyBoard();
                    showDialogForFormula();
                }

            }
        });
    }





    /**
     *  hide KeyBoard when Button Click
     */
    private void hideKeyBoard() {
        // Check if no view has focus:
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    /**
     *  clear All Data from List
     */

    private void clearAllData() {
        if(deleteCharList!=null && !deleteCharList.isEmpty()){
            deleteCharList.clear();
        }
        if(insertCharGenerateCountList!=null && !insertCharGenerateCountList.isEmpty()){
            insertCharGenerateCountList.clear();
        }
        if(swapCharGenerateList!=null && !swapCharGenerateList.isEmpty()){
            swapCharGenerateList.clear();
        }
        if(swapAdjacentCharGenerateList!=null && !swapAdjacentCharGenerateList.isEmpty()){
            swapAdjacentCharGenerateList.clear();
        }
    }


    /**
     *  get word from Edittext
     * @return
     */
    @NonNull
    private String getWord() {
        return etEnterWordForComputation.getText().toString();
    }


    /**
     *  validate Data
     * @return
     */
    private boolean checkInfoValid(){
        if(getWord().trim().equalsIgnoreCase("")){
            Toast.makeText(context,"Please enter word ",Toast.LENGTH_LONG).show();
            return false;
        }


        return true;
    }

    private void idMappings() {
        etEnterWordForComputation=(EditText) findViewById(R.id.etEnterWordForPermutation);
        btnSubmit=(AppCompatButton) findViewById(R.id.btnSubmit);

        btnDeleteChar =(AppCompatButton) findViewById(R.id.tvDeleteChar);
        btnInsertChar =(AppCompatButton) findViewById(R.id.tvInsertChar);
        btnReplaceChar =(AppCompatButton) findViewById(R.id.tvReplaceChar);
        btnSwapChar =(AppCompatButton) findViewById(R.id.tvSwapChar);

        btnNoOfGeneratedWord =(AppCompatButton) findViewById(R.id.btnNoOfGeneratedWord);
        btnFormula =(AppCompatButton) findViewById(R.id.btnFormula);


        progressBar=(ProgressBar) findViewById(R.id.progressBar);

    }


    /**
     *  Delete One Letter from Word
     *
     */
    private void  deleteOneLetterFromWord(String word){
        for (int i=0;i<word.length();i++){
            StringBuilder sb = new StringBuilder(word);
            String newWord = sb.deleteCharAt(i).toString();
            deleteCharList.add(newWord);
        }
    }


    /**
     *  insert Letter in word
     * @param word
     *
     */
    private void  insertLetterInWord(String word){
        for (int i=0;i<=word.length();i++){
            for(Character ch = 'a' ; ch <= 'z' ; ch++ ) {
                String newWord = new StringBuilder(word).insert(i, ch).toString();
                insertCharGenerateCountList.add(newWord);
            }
        }
    }


    /**
     *  replace One Letter with other
     * @param word
     */
    private void  replaceOneLetterWithOther(String word){
        for (int i=0;i<word.length();i++){
            StringBuilder stringBuilder=new StringBuilder(word);
            for(Character ch = 'a' ; ch <= 'z' ; ch++ ) {
                stringBuilder.setCharAt(i, ch);
                String newWord = stringBuilder.toString();
                swapCharGenerateList.add(newWord);
            }
        }


    }


    /**
     *  get Swap Adjacent
     * @param word
     */
    private   void getSwapAdjacentValues(String word) {
       for (int i = 0; i < word.length() - 1; i++) {
                StringBuilder sb = new StringBuilder();
                char a = word.charAt(i);
                char b = word.charAt(i + 1);
                String newWord;
                if(i!=0){
                    newWord = word.substring(0,i) + sb.append(b).append(a) + word.substring((i + 2));
                }else {
                    newWord = sb.append(b).append(a) + word.substring((i + 2));
                }
                swapAdjacentCharGenerateList.add(newWord);
        }

    }




    public String  swapAdjacent(String word){
        StringBuilder output = new StringBuilder();
        char[] characters = word.toCharArray();
        for (int i = 0; i < characters.length; i++) {
            if (i % 2 == 0) {
                if((i+1) < characters.length ) {
                    output.append(characters[i + 1]);
                }
                output.append(characters[i]);
            }
        }

        return output.toString();
    }


    /**
     *  Done Calcualtion in Background
     */
    private  class  FetchDataAsyncTask extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            clearAllData();
            deleteOneLetterFromWord(getWord());
            insertLetterInWord(getWord());
            replaceOneLetterWithOther(getWord());
//                    swapAdjacent(getWord());
            getSwapAdjacentValues(getWord());
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);

        }

    }


    /**
     *  show Dialog
     * @param title
     * @param formula
     * @param list
     */
    private void  showDialog(String title,String formula,List<String> list){

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_custom_layout);
        RecyclerView rvItemList=(RecyclerView) dialog.findViewById(R.id.rvItemList);
        TextView tvChar=(TextView) dialog. findViewById(R.id.tvChar);
        TextView tvFormula=(TextView) dialog. findViewById(R.id.tvFormulaChar);

        tvChar.setText(title);
        tvFormula.setText(formula);


        // set Layout Manager
        RecyclerView.LayoutManager layoutManagerItemList = new GridLayoutManager(context,4);
        rvItemList.setLayoutManager(layoutManagerItemList);

        // set Data in RecyclerView Adapter
        ItemListAdapter adapter = new ItemListAdapter(context, list);
        rvItemList.setAdapter(adapter);

        dialog.show();

    }


    /**
     *  show Dialog for Count
     */
    private void  showDialogForCount(){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_custom_count_layout);


        TextView tvDeleteCharListCount=(TextView) dialog. findViewById(R.id.tvDeleteCharListCount);
        TextView tvInsertCharListCount=(TextView) dialog. findViewById(R.id.tvInsertCharListCount);
        TextView tvReplaceCharListCount=(TextView) dialog. findViewById(R.id.tvReplaceCharListCount);
        TextView tvSwapCharListCount=(TextView) dialog. findViewById(R.id.tvSwapCharListCount);

        // set Count
        tvDeleteCharListCount.setText(String.format(Locale.ENGLISH,"%5s%d"," ",deleteCharList.size()));
        tvInsertCharListCount.setText(String.format(Locale.ENGLISH,"%5s%d", " ",insertCharGenerateCountList.size()));
        tvReplaceCharListCount.setText(String.format(Locale.ENGLISH,"%5s%d", " ",swapCharGenerateList.size()));
        tvSwapCharListCount.setText(String.format(Locale.ENGLISH,"%5s%d", " ",swapAdjacentCharGenerateList.size()));


        dialog.show();

    }


    /**
     *  show Dialog For Formula
     */
    private void  showDialogForFormula(){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_custom_forula_layout);

        TextView tvDeleteCharListCount=(TextView) dialog. findViewById(R.id.tvDeleteCharListFormula);
        TextView tvInsertCharListCount=(TextView) dialog. findViewById(R.id.tvInsertCharListFormula);
        TextView tvReplaceCharListCount=(TextView) dialog. findViewById(R.id.tvReplaceCharListFormula);
        TextView tvSwapCharListCount=(TextView) dialog. findViewById(R.id.tvSwapCharListFormula);


        // write formula
        tvDeleteCharListCount.setText(R.string.no_of_chars);
        tvInsertCharListCount.setText(R.string.insert_char_formula);
        tvReplaceCharListCount.setText(R.string.replace_char_formula);
        tvSwapCharListCount.setText(R.string.swap_formula);

        dialog.show();

    }






}